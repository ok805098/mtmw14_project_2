#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: 28805098
"""

import numpy as np
import matplotlib.pyplot as plt
from __init__ import *


def analytic():
    """
    analytic solution for gyre
    """
    
    epsilon = gamma / (L * beta)
    a = (-1 - np.sqrt(1 + (2 * np.pi * epsilon)**2)) / (2 * epsilon)
    b = (-1 + np.sqrt(1 + (2 * np.pi * epsilon)**2)) / (2 * epsilon)
    
    u_st = np.zeros((arraysize, arraysize))
    v_st = np.zeros((arraysize, arraysize))
    eta_st = np.zeros((arraysize, arraysize))
    
    xrange = range(arraysize)
    yrange = range(arraysize)
    
    for i in xrange:
        for j in yrange:
            
            def f1(x):
                f1 = np.pi * (1 + ((np.exp(a) - 1) * np.exp(b * x) + (1 - np.exp(b)) \
                                * np.exp(a * x)) / (np.exp(b) - np.exp(a)))
                return f1
            
            def f2(x):
                f2 = ((np.exp(a) - 1) * b * np.exp(b * x) + (1 - np.exp(b)) * a \
                      * np.exp(a * x)) / (np.exp(b) - np.exp(a))
                return f2
            
            u_st[j,i] = -tau0 / (np.pi * gamma * rho * H) * f1(d*i / L) * \
                np.cos(np.pi * d*j / L)
            
            v_st[j,i] = tau0 / (np.pi * gamma * rho * H) * f2(d*i / L) * \
                np.sin(np.pi * d*j / L)
            
            eta_st[j,i] = eta0 + (tau0 / (np.pi * gamma * rho * H)) * (f0 * L / g) * \
                (((gamma / (f0 * np.pi)) * f2(d*i/L) * np.cos(np.pi * d*j / L)) \
                    + (1 / np.pi) * f1(d*i/L) * (np.sin(np.pi * d*j / L) * (1 + (beta * d*j)/f0) + \
                                            (beta * L / (f0 * np.pi)) * np.cos(np.pi * d*j / L)))

        
    plt.contourf(u_st, levels= np.arange(np.amin(u_st),np.amax(u_st)+0.001,(np.amax(u_st) - np.amin(u_st))/10))
    plt.colorbar()
    plt.show()  
    plt.contourf(v_st)
    plt.colorbar()
    plt.show() 
    plt.contourf(eta_st)
    plt.colorbar()
    plt.show()
    # plt.contourf(eta_st[:,0:20]) 
    # plt.show()       
    
    print("a = ", a)
    print("b = ", b)
    print("epsilon = ", epsilon)
    print("max/min u_st = ", np.amax(u_st), np.amin(u_st))
    print("max/min v_st = ", np.amax(v_st), np.amin(v_st))
    print("max/min eta_st = ", np.amax(eta_st), np.amin(eta_st))
    
    return u_st, v_st, eta_st
   
                   
                        
               
                        
                        
                        
                        
                        