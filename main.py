#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: 28805098
"""

from __init__ import *
from analytic import *
from numerical import *
from energy_integral import *

def main():
    u_st, v_st, eta_st = analytic()
    new_u, new_v, new_eta, total_energy_time = numerical()
    
    u_diff = new_u[1:-1, 1:-1] - u_st
    v_diff = new_v[1:-1, 1:-1] - v_st
    eta_diff = new_eta[1:-1, 1:-1] - eta_st
    
    print('u_diff: ', np.shape(u_diff))
    print('v_diff: ', np.shape(v_diff))
    print('eta_diff: ', np.shape(eta_diff))
    
    total_energy_diff = energy(u_diff, v_diff, eta_diff)
    total_energy_num = energy(new_u, new_v, new_eta)
    total_energy_st = energy(u_st, v_st, eta_st)
    
    print('total energy difference field: ', total_energy_diff)
    print('total energy steady state: ', total_energy_st)
    print('total energy final timestep: ', total_energy_num)
    
    time = np.linspace(0,t,nt)
    plt.plot(time, total_energy_time)
    plt.ylabel('Energy (J)')
    plt.xlabel('Seconds')
    plt.title('Total energy time series')
    

    return total_energy_time

total_energy_time = main()
    
    