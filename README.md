# MTMW14_Project_2

### THIS IS THE UP TO DATE .pdf COMMITTED 20:10 30/03/21 ###
(only extra couple on sentences at start of page 4)

A simplified model of the ocean gyres and gulf stream.

analytic.py : calculated the analytic solution and plots u,v and eta 

numerical.py : computed numerical solution and produces relating plots

energy_integral.py : computed the energy integral for given inputs of u,v and eta

__init__.py : store of constants

main.py : calls all functions to produce plots and outputs required for Project