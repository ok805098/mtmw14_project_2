#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: 28805098
"""

import numpy as np
from __init__ import *
import matplotlib.pyplot as plt
from energy_integral import *

def numerical():
    """
    numerical scheme for ocean gyre, calculates total energy at each timestep
    """

    #create zero arrays for u,v and eta with outer 'buffer' 
    new_u = np.zeros((arraysize+2,arraysize+2))
    new_v = np.zeros((arraysize+2,arraysize+2))
    new_eta = np.zeros((arraysize+2,arraysize+2))
    
    old_u = np.zeros((arraysize+2,arraysize+2))
    old_v = np.zeros((arraysize+2,arraysize+2))
    old_eta = np.zeros((arraysize+2,arraysize+2))
    
    total_energy_time = np.zeros([nt])
    
    oddtimestep = True #use to toggle odd and even timesteps
    
    for n in range(nt): #stepping through time
        for j in range(1, arraysize+1): #stepping through rows, y direction
            for i in range(1, arraysize+1): #stepping through columns, x direction
                dudx = (old_u[j,i+1] - old_u[j,i])/ d
                dvdy = (old_v[j+1,i] - old_v[j,i])/ d 
                new_eta[j,i] = old_eta[j,i] - (H * dt) * (dudx + dvdy) 
                
        if oddtimestep:  #odd timestep
            #calculate new u
            for j in range(1, arraysize+1): #stepping through rows, y direction
                tau = [-np.cos(np.pi * j * d / L) * tau0 , 0] #tau calculated each j step
                for i in range(1, arraysize+1): #stepping through columns, x direction             
                    detadx = (new_eta[j,i] - new_eta[j,i-1])/d
                    fv = (f0 + beta * j * d) * (old_v[j,i] + old_v[j+1,i] + old_v[j,i-1] + old_v[j+1,i-1])/4
                    new_u[j,i] = old_u[j,i] +  dt * fv  - \
                        g * dt * detadx - gamma * dt * old_u[j,i] + tau[0] * dt / (rho * H)
            #set up the outer buffer so that the flux = 0 and the average (fu and fv) are easily calculated
            # new_u[1:arraysize,0] = new_u[1:arraysize,2] #set left boundary buffer 
            # new_u[1:arraysize,1] = new_u[1:arraysize,2] #set left inner buffer
            # new_u[1:arraysize,-1] = new_u[1:arraysize,-2] #set right boundary buffer
            # new_u[0,1:arraysize] = new_u[2,1:arraysize] #set top boundary buffer
            # new_u[1,1:arraysize] = new_u[2,1:arraysize] #set top inner buffer
            # new_u[-1,1:arraysize] =  new_u[-2,1:arraysize] #set bottom boundary buffer
            # new_u[0,0] = new_u[1,1] #set corners
            # new_u[-1,0] = new_u[-2,1]
            # new_u[0,-1] = new_u[1,-2]
            # new_u[-1,-1] = new_u[-2,-2]
            #calculate new v
            for j in range(1, arraysize+1): #stepping through rows, y direction
                tau = [-np.cos(np.pi * j * d / L) * tau0 , 0] #tau calculated each j step
                for i in range(1, arraysize+1): #stepping through columns, x direction
                    detady = (new_eta[j,i] - new_eta[j-1,i])/d
                    fu = (f0 + beta * j * d) * (new_u[j,i] + new_u[j,i+1] + new_u[j-1,i] + new_u[j-1,i+1])/4
                    new_v[j,i] = old_v[j,i] - dt * fu  - \
                        g * dt * detady - gamma * dt * old_v[j,i] + tau[1] * dt / (rho * H)
            #set up the outer buffer so that the flux = 0 and the average (fu and fv) are easily calculated
            # new_v[1:arraysize,0] = new_v[1:arraysize,2] #set left boundary buffer
            # new_v[1:arraysize,1] = new_v[1:arraysize,2] #set left inner buffer
            # new_v[1:arraysize,-1] = new_v[1:arraysize,-2] #set right boundary buffer
            # new_v[0,1:arraysize] = new_v[2,1:arraysize] #set top boundary buffer
            # new_v[1,1:arraysize] = new_v[2,1:arraysize] #set top inner buffer
            # new_v[-1,1:arraysize] =  new_v[-2,1:arraysize] #set bottom boundary buffer
            # new_v[0,0] = new_v[1,1] #set corners
            # new_v[-1,0] = new_v[-2,1]
            # new_v[0,-1] = new_v[1,-2]
            # new_v[-1,-1] = new_v[-2,-2]
            
        else: #even timestep
            #calculate new v
            for j in range(1, arraysize+1): #stepping through rows, y direction
                tau = [-np.cos(np.pi * j * d / L) * tau0 , 0] #tau calculated each j step
                for i in range(1, arraysize+1): #stepping through columns, x direction              
                    detady = (new_eta[j,i] - new_eta[j-1,i])/d
                    fu = (f0 + beta * j * d) * (old_u[j,i] + old_u[j,i+1] + old_u[j-1,i] + old_u[j-1,i+1])/4
                    new_v[j,i] = old_v[j,i] - dt * fu  - \
                        g * dt * detady - gamma * dt * old_v[j,i] + tau[1] * dt / (rho * H)
            #set up the outer buffer so that the flux = 0 and the average (fu and fv) are easily calculated
            # new_v[1:arraysize,0] = new_v[1:arraysize,2] #set left boundary buffer
            # new_v[1:arraysize,1] = new_v[1:arraysize,2] #set left inner buffer
            # new_v[1:arraysize,-1] = new_v[1:arraysize,-2] #set right boundary buffer
            # new_v[0,1:arraysize] = new_v[2,1:arraysize] #set top boundary buffer
            # new_v[1,1:arraysize] = new_v[2,1:arraysize] #set top inner buffer
            # new_v[-1,1:arraysize] =  new_v[-2,1:arraysize] #set bottom boundary buffer
            # new_v[0,0] = new_v[1,1] #set corners
            # new_v[-1,0] = new_v[-2,1]
            # new_v[0,-1] = new_v[1,-2]
            # new_v[-1,-1] = new_v[-2,-2]
            #calculate new u
            for j in range(1, arraysize+1): #stepping through rows, y direction
                tau = [-np.cos(np.pi * j * d / L) * tau0 , 0] #tau calculated each j step
                for i in range(1, arraysize+1): #stepping through columns, x direction              
                    detadx = (new_eta[j,i] - new_eta[j,i-1])/d
                    fv = (f0 + beta * j * d) * (new_v[j,i] + new_v[j+1,i] + new_v[j,i-1] + new_v[j+1,i-1])/4
                    new_u[j,i] = old_u[j,i] +  dt * fv  - \
                        g * dt * detadx - gamma * dt * old_u[j,i] + tau[0] * dt / (rho * H)
             #set up the outer buffer so that the flux = 0 and the average (fu and fv) are easily calculated
            # new_u[1:arraysize,0] = new_u[1:arraysize,2] #set left boundary buffer 
            # new_u[1:arraysize,1] = new_u[1:arraysize,2] #set left inner buffer
            # new_u[1:arraysize,-1] = new_u[1:arraysize,-2] #set right boundary buffer
            # new_u[0,1:arraysize] = new_u[2,1:arraysize] #set top boundary buffer
            # new_u[1,1:arraysize] = new_u[2,1:arraysize] #set top inner buffer
            # new_u[-1,1:arraysize] =  new_u[-2,1:arraysize] #set bottom boundary buffer
            # new_u[0,0] = new_u[1,1] #set corners
            # new_u[-1,0] = new_u[-2,1]
            # new_u[0,-1] = new_u[1,-2]
            # new_u[-1,-1] = new_u[-2,-2]
        
        #calculate energy integral
        total_energy_time[n] = energy(new_u, new_u, new_u)
        
        #for toggle between odd/even
        oddtimestep = not oddtimestep  
        
        #reset variables
        old_u = new_u
        old_v = new_v
        old_eta = new_eta
    
        print("max u = ", np.amax(new_u))
        print("min u = ", np.amin(new_u))
        print("max v = ", np.amax(new_v))
        print("min v = ", np.amin(new_v))
        print("max eta = ", np.amax(new_eta))
        print("min eta = ", np.amin(new_eta))
        print("timestep=",n)
                
        
    plt.plot(range(arraysize+2), new_u[-2,:])
    plt.ylabel('$ms^{-1}$')
    plt.xlabel('$x$ (* 50km)')
    plt.title('$u$ along southern edge of basin')
    plt.show()
    plt.plot(range(arraysize+2), new_v[:,1])
    plt.ylabel('$ms^{-1}$')
    plt.xlabel('$y$ (* 50km)')
    plt.title('$v$ along western edge of basin')
    plt.show()
    middle = int((arraysize+2)/2)
    plt.plot(range(arraysize+2), new_eta[middle,:])
    plt.ylabel('$m$')
    plt.xlabel('$x$ (* 50km)')
    plt.title('$\eta$ through the middle of the basin')
    plt.show()
    
    plt.contourf(new_eta[:,:])
    plt.colorbar() 
    plt.show() 
    plt.contourf(new_u[:,:])
    plt.colorbar() 
    plt.show() 
    plt.contourf(new_v[:,:])
    plt.colorbar() 
    plt.show()
    
    plt.contourf(new_eta[:,:])
    plt.colorbar() 
    x,y = np.meshgrid(np.linspace(0,arraysize+1, arraysize+2),np.linspace(0,arraysize+1, arraysize+2))
    plt.quiver(x,y,new_u,new_v)
    plt.show()
    
    return new_u, new_v, new_eta, total_energy_time

# new_u, new_v, new_eta = numerical()
         
                
                




              

