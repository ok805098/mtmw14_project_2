#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: 28805098
"""

from __init__ import *
import numpy as np
from scipy.integrate import simps

def energy(u,v,eta):
    """
    calculated the energy integral
    """
    #create arrays for integration
    x = np.linspace(0,L,len(u[1]))
    y = np.linspace(0,L,len(u[0]))
    
    #function to be integrated
    def E(u, v, eta):
        return 1/2 * rho * (H * (u**2 + v**2) + g * eta**2)
    
    E = E(u, v, eta)
    
    #integrate function
    total_energy = simps([simps(E_x,x) for E_x in E],y)
    
    return total_energy
                
