#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: 28805098
"""

"""File of constants used in Project 2"""

d = 50000 #metres
f0 = 1e-4 #(1/s) 
beta = 1e-11 #(1/ms)
g = 10 #(m/s^2) gravitational acceleration
gamma = 1e-6 #(1/s) linear drag coefficient
rho = 1000 #(kg/m^3) uniform density
H = 1000 #(m) resting depth of fluid
L = 1000000 #(m) length of domain
tau0 = 0.2 #(N/m^2) initial wind stress acting on the surface of the fluid
eta0 = 0.035
arraysize = int(L/d)+1
t = 86400 #second in a day
dt = 250 #length of timestep
nt = int(t/dt) #number of timesteps

